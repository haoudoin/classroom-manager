var app = angular.module('classroom-manager', ["ui.sortable", "ui.tinymce", "ui.calendar", "ngAnimate", "ngRoute"]);


app.run(function ($rootScope, $q) {
    $rootScope.userLoaded = false;

    var getUser = function () {
        var deferred = $q.defer();
        dpd.users.me(function (user) {
            $rootScope.$apply(function(){
                $rootScope.currentUser = user;
                $rootScope.userLoaded = true;
                deferred.resolve();
//                console.log("finish");
            })

        });
//        console.log("return")
        return deferred.promise;
    }

    $rootScope.handleIndicator = function (e, $scope, element) {
//        console.log("handleit")
        $(element).find(".active").removeClass("active");
        $(this).addClass("active");
    };

    $rootScope.isAdmin = function () {
//        console.log($rootScope.currentUser !== undefined && $rootScope.currentUser.role == "admin")
        return (($rootScope.currentUser && $rootScope.currentUser.role == "admin") ? true : false);
    };

    $rootScope.getUser = getUser;

    var promise = getUser();

//    console.log("after getUser();")
});