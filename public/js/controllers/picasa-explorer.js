/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/22/13
 * Time: 9:52 PM
 */

//https://picasaweb.google.com/data/feed/projection/user/userID/albumid/albumID?kind=kinds

app.controller("PicasaExplorerController", function ($scope, galleries) {

    var json_Album_URI = "https://picasaweb.google.com/data/feed/base/"
        + "user/" + "haofengtan"
        + "?alt=" + "json"
        + "&kind=" + "album"
        + "&hl=" + "en_US"
        + "&fields=" + "entry(media:group,id)"
        + "&thumbsize=" + 104;

    $scope.albums = [];

    function loadAlbum(json_Photo_URI) {
//        console.log("loading album", json_Photo_URI);

        $.getJSON(json_Photo_URI, function (data) {
            $scope.currentAlbum = normalizePhotoAlbum(data);
            $scope.$apply();
        });
    }

    function normalizePhotoAlbum(picasDataItem) {

        var photos = [];

        $.each(picasDataItem.feed.entry, function (i, item) {
//            console.log("picasa photo item: ", item)
//                    $('#images').append("Album Photos: <br />");
            //Photo URL
            if (item.media$group.media$content.length <= 1) {

                var photo_URL, photo_Thumb_URL, photo_width, photo_height;
                $.each(item.media$group.media$content, function (i, item) {
                    photo_URL = item.url;
                    photo_width = item.width;
                    photo_height = item.height;
//                        $('#images').append("Image Photo URL " + i+": " + photo_URL + "<br />");
                });
                //Thumbnail URL
                $.each(item.media$group.media$thumbnail, function (i, item) {
                    photo_Thumb_URL = item.url;
//                        $('#images').append("Image Thumbnail URL: <img src=\" " + photo_Thumb_URL + "\" /><br />");
                });
                //Photo Title
                var photo_Title = item.media$group.media$title.$t;
//                    $('#images').append("Image Photo_Title: " + photo_Title + '<br />');
                //Photo Description
                var photo_Description = item.media$group.media$description.$t;
//                    $('#images').append("Image Photo Description: " + photo_Description + '<br /><br />');
                var photo = {
                    title: photo_Title,
                    description: photo_Description,
                    url: photo_URL,
                    thumbnail: photo_Thumb_URL,
                    width: photo_width,
                    height: photo_height
                };
                photos.push(photo);
            }
        });
//        console.log("normalized photos: ", photos);
        return photos;
    };

    var _loadOnce;

    if (galleries && galleries.data && galleries.data.length > 0) {
        loadAlbum(galleries.data[0].url);
    } else {
        _loadOnce = loadAlbum;
    }

    $scope.loadAlbums = function () {
        $.getJSON(json_Album_URI, function (data) {
            $.each(data.feed.entry, function (i, item) {
                // normalize data into more readable properties
                //Thumbnail URL
                var album_thumb_URL;
                $.each(item.media$group.media$thumbnail, function (i, item) {
                    console.log("picasa album item", item);
                    album_thumb_URL = item.url;
                });
                //Album Title
                var album_Title = item.media$group.media$title.$t;
                //Album Description
                var album_Description = item.media$group.media$description.$t;
                //Album ID
                var album_ID = item.id.$t;
                //Get Numerical ID from URL
                album_ID = album_ID.split('/')[9].split('?')[0];

                var json_Photo_URI = "https://picasaweb.google.com/data/feed/base/"
                    + "user/" + "haofengtan@gmail.com"
                    + "/albumid/" + album_ID
                    + "?alt=" + "json"
                    + "&kind=" + "photo"
                    + "&hl=" + "en_US"
                    + "&fields=" + "entry(media:group)"
                    + "&thumbsize=" + 104;


                if (typeof _loadOnce === "function") {
                    _loadOnce(json_Photo_URI);
                }
                _loadOnce = null;
                var album = {
                    id: album_ID,
                    title: album_Title,
                    thumbnail: album_thumb_URL,
                    description: album_Description
                };

                $scope.albums.push(album);

            });
//            console.log("albums: ", $scope.albums);
            $scope.$apply();

        });
    }

});
