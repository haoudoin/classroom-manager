//  $(document).ready(function () {
app.controller('ChatRoomController', function ($scope, $http, messages) {

    var $ = jQuery;
//    var deferred = $q.defer();

    dpd.messages.on('create', function (message) {
        renderMessage(message);
    });

//    dpd.messages.get(function (messages) {
//        if (messages) {
//            messages.forEach(function (m) {
//                renderMessage(m);
//            });
//        }
//    });

    $scope.messages = messages.data;

   // $('#send-btn').click(sendMessage);

    function renderMessage(message) {
        var $el = $('<li>');
        $el.append('<strong>' + message.sender + ': </strong>');
        $el.append(message.message);
        $el.appendTo('#chatbox');
        $scope.$apply();
    }

    $scope.sendMessage = function () {
        dpd.users.me(function (user, error) {
//            console.log(user)
            var message = $('#message').val();
            dpd.messages.post({
                sender: user.username,
                message: message
            }, function (message, err) {
                if (err) {
                    if (err.message) {
                        alert(err.message);
                    } else if (err.errors) {
                        var errors = "";
                        if (err.errors.sender) {
                            errors += err.errors.sender + "\n";
                        }
                        if (err.errors.message) {
                            errors += err.errors.message + "\n";
                        }
                        alert(errors);
                    }
                } else {
                    $('#message').val('');
                }
            });
        });
        return false;
    }

});

