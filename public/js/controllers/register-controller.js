app.controller('RegisterController', function ($scope, $rootScope) {
    if (document.referrer && document.referrer !== location.href) {
        $scope.referrer = document.referrer;
    } else {
        $scope.referrer = '/';
    }

    $scope.register = function () {
        $scope.showError=false;

        if ($scope.password !== $scope.confirmPassword) {
            $scope.showError=true;
            $rootScope.errorMessage = "Passwords do not match";
            return false;
        }

        dpd.users.post({
            username: $scope.username,
            password: $scope.password,
            emailAddress: $scope.emailAddress,
            assignedId: $scope.assignedId,
            firstName: $scope.firstName,
            lastName: $scope.lastName
        }, function (user, error) {
            if (error) {
                if (error.message) {
                    $scope.showError=true;
                    $rootScope.errorMessage = messages;
                } else if (error.errors) {
                    var messages = '';
                    var errors = error.errors;

                    if (errors.username) {
                        messages += "Username " + errors.username + "\n";
                    }
                    if (errors.password) {
                        messages += "Password " + errors.password + "\n";
                    }

//                    alert(messages);
                    $scope.showError=true;
                    $rootScope.errorMessage = messages;
                }
            } else {
                dpd.users.login({
                    username: $scope.username,
                    password: $scope.password
                }, function () {
                    location.href = $scope.referrer;
                });
            }
        });
        return false;
    };

});