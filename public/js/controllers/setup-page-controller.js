/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/15/13
 * Time: 8:41 PM
 */

app.controller("PageSetupController", function ($scope) {
    var query = location.search;
    var page = $scope.page.alias;
    $scope.tinymceOptions = {
        plugins: [
            "advlist autolink lists link image charmap preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"

    }

    $scope.savePage = function (updatedPage) {
        console.log(updatedPage)
        dpd("default-pages").put(updatedPage, function(){
            $scope.$apply();
        })
    }

});

