app.factory('Feed', function ($rootScope) {
    var PAGE_SIZE = 5;

    var Feed = function Feed(query) {
        this.query = query || {};
        this.posts = [];
        this.lastTime = 0;
        this.moreToLoad = false;
    };

    Feed.prototype.loadPosts = function () {
        var feed = this;

        var query = angular.copy(this.query);
        query.$limit = PAGE_SIZE + 1;
        query.postedTime = {$lt: this.lastTime};
        query.$sort = {postedTime: -1};
//        console.log("loadPosts: ", query);

        dpd.posts.get(query, function (result) {
            if (result.length > PAGE_SIZE) {
                result.pop();
                feed.moreToLoad = true;
            } else {
                feed.moreToLoad = false;
            }
            if (result.length) feed.lastTime = result[result.length - 1].postedTime;

            Array.prototype.push.apply(feed.posts, result);

            $rootScope.$apply();
        });
    };

    Feed.prototype.refresh = function () {
        this.lastTime = new Date().getTime();
        this.posts.length = 0;
        this.loadPosts();
        this.moreToLoad = false;
    };

    return Feed;
});


app.controller('FeedController', function ($scope, $rootScope, Feed) {

    var PAGE_SIZE = 5;
    var isPoster = false;

    var feed = new Feed();
    $scope.feed = feed;
    $scope.tinymceOptions = {
        plugins: [
            "advlist autolink lists link image charmap preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"

    }

//    console.log($rootScope.currentUser);
    dpd.users.me(function(result,error) {
        // console.log(result)
        $scope.postable = (result.role == "admin");

        $scope.$apply();
    });
    $scope.submit = function (newPost) {
        isPoster = true;

        dpd.posts.post({
            message: newPost
        }, function (result, error) {
            if (error) {
                if (error.message) {
                    alert(error.message);
                } else if (error.errors && error.errors.message) {
                    alert("Message " + error.errors.message);
                } else {
                    alert("An error occurred");
                }
            } else {
                feed.posts.unshift(result);
                $scope.newPost = '';
                $scope.$apply();
            }
        });
    };

    dpd.on("posts:create", function(){
        if(!isPoster) {
            feed.refresh();
            isPoster = false;
        }
    });
    feed.refresh();

});