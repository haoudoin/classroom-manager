/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/17/13
 * Time: 11:09 PM
 */
app.controller('CalendarController', function ($scope, events) {
    /* config object */
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /* alert on eventClick */
    $scope.alertEventOnClick = function (date, allDay, jsEvent, view) {
        $scope.$apply(function () {
            $scope.alertMessage = ('Day Clicked ' + date);
        });
    };
    /* alert on Drop */
    $scope.alertOnDrop = function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
        $scope.$apply(function () {
            $scope.alertMessage = ('Event Droped to make dayDelta ' + dayDelta);
        });
    };
    /* alert on Resize */
    $scope.alertOnResize = function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
        $scope.$apply(function () {
            $scope.alertMessage = ('Event Resized to make dayDelta ' + minuteDelta);
        });
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function (sources, source) {
        var canAdd = 0;
        angular.forEach(sources, function (value, key) {
            if (sources[key] === source) {
                sources.splice(key, 1);
                canAdd = 1;
            }
        });
        if (canAdd === 0) {
            sources.push(source);
        }
    };
    /* add custom event*/
    $scope.addEvent = function (event) {
        $scope.events = event;
    };
    /* remove event */
    $scope.remove = function (index) {
        $scope.events.splice(index, 1);
    };
    /* Change View */
    $scope.changeView = function (view, calendar) {

        $(this).addClass("active")
        $($scope.siteCalendar).fullCalendar('changeView', view);
    };
    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 678,
            editable: true,
            header: {
                left: 'title',
                center: '',
                right: 'today prev,next'
            }
//            ,
//            dayClick: $scope.alertEventOnClick,
//            eventDrop: $scope.alertOnDrop,
//            eventResize: $scope.alertOnResize
        }
    };
//    console.log(datasets.data);
    /* event sources array*/
//    $scope.eventSources = [datasets.data, $scope.siteEventsFn];
    $scope.eventSources = events.data;

});
