/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/15/13
 * Time: 6:56 PM
 */


app.controller('LoginCtrl', function ($scope, $rootScope, $location) {


    $scope.showLogin = function (val) {
        $scope.loginVisible = val;
        if (val) {
            $scope.username = '';
            $scope.password = '';
        }
    };

    $scope.login = function () {
//        console.log("login");
        dpd.users.login({
            username: $scope.username,
            password: $scope.password
        }, function (session, error) {

            if (error) {
                alert(error.message);
            } else {
                $scope.showLogin(false);

                var promise = $rootScope.getUser();
//                promise.then(function(){console.log("finish getUser();");})

            }
        });
    };

    $scope.logout = function () {
        dpd.users.logout(function () {
            $scope.$apply(function(){
                $rootScope.currentUser = null;
            });
        });
    };
});
