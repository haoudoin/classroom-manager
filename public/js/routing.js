/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/15/13
 * Time: 7:40 PM
 */
app.config(function ($routeProvider, $locationProvider) {

    $routeProvider.when('/', {
        templateUrl: 'partials/welcome.html'
//        , resolve: {
//            // I will cause a 1 second delay
//            delay: function ($q, $timeout) {
//                var delay = $q.defer();
//                $timeout(delay.resolve, 750);
//                return delay.promise;
//            }
//        }
    });
    $routeProvider.when('/register.html', {
        templateUrl: 'partials/register.html',
        controller: 'RegisterController'
//        , resolve: {
//            // I will cause a 1 second delay
//            delay: function ($q, $timeout) {
//                var delay = $q.defer();
//                $timeout(delay.resolve, 750);
//                return delay.promise;
//            }
//        }

    });
    $routeProvider.when('/setup.html', {
        templateUrl: 'partials/setup-carousel.html',
        controller: 'SetupController'
//        , resolve: {
//            // I will cause a 1 second delay
//            delay: function ($q, $timeout) {
//                var delay = $q.defer();
//                $timeout(delay.resolve, 750);
//                return delay.promise;
//            }
//        }

    });
    $routeProvider.when('/profile.html', {
        templateUrl: 'partials/profile.html',
        controller: "UserFeedController"
    });
    $routeProvider.when('/announcements.html', {
        templateUrl: 'partials/announcements.html',
        controller: "FeedController"
//        ,resolve: {
//            // I will cause a 1 second delay
//            delay: function ($q, $timeout) {
//                var delay = $q.defer();
//                $timeout(delay.resolve, 750);
//                return delay.promise;
//            }
//        }

    });
    $routeProvider.when('/chat.html', {
        templateUrl: 'partials/chat.html',
        controller: "ChatRoomController",
        resolve: {
            messages: function ($http) {
                return $http({
                    method: 'GET',
                    url: '/messages'
                });
            }
        }

    });
    $routeProvider.when('/to-do.html', {
        templateUrl: 'partials/to-do.html',
        controller: "TodoController"
    });
    $routeProvider.when('/calendar.html', {
        templateUrl: 'partials/calendar.html',
        controller: "CalendarController",
        resolve: {
            events: function ($http) {
                return $http({
                    method: 'GET',
                    url: '/calendar-feeds'
                });
            }
        }
    });

    $routeProvider.when('/gallery.html', {
        templateUrl: 'partials/gallery.html',
        controller: "PicasaExplorerController",
        resolve: {
            galleries: function ($http) {
                return $http({
                    method: "GET",
                    url: "/photo-galleries"
                });
            }
        }
    });
    //configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode(false).hashPrefix("!");
});

