/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/15/13
 * Time: 7:44 PM
 */

app.directive('navMenu', function ($location) {
    return function (scope, element, attrs) {
        console.log($location);
        var links = element.find('a'),
            link,
            currentLink,
            urlMap = {},
            i;

        for (i = 0; i < links.length; i++) {
            link = angular.element(links[i])
            urlMap[link.attr('href')] = link;
        }

//        console.log($location.path(), urlMap);
        currentLink = urlMap["/#" + $location.path()];
        if (!currentLink) currentLink = urlMap["/#"];

        if(currentLink) {
            currentLink.parent().addClass("active");
        }

        scope.$on('$routeChangeStart', function () {

            var pathLink = urlMap["/#" + $location.path()];

            if (pathLink) {
                if (currentLink) {
                    currentLink.parent().removeClass('active');
                }
                currentLink = pathLink;
                currentLink.parent().addClass('active');
            }
        });
    };
});