/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/16/13
 * Time: 12:40 AM
 */
app.directive('delegate', function(){
    return function($scope, element, attrs) {
        var fn = attrs.delegate;
        var event = attrs.delegateEvent;
//        console.log(fn, event);
        $(element).on(event, attrs.delegateSelector, function(e){
//            console.log(this);
            var data = angular.fromJson( angular.element( e.target ).data('ngJson') || undefined );
            if( typeof $scope[ fn ] == "function" ) $scope[ fn ].call(this, e, $scope, element, data );
        });
    };
});