///**
// * Created with IntelliJ IDEA.
// * User: Hao
// * Date: 9/16/13
// * Time: 10:03 PM
// */
//app.directive("siteNav", function(){
//    var directiveDefinitionObject = {
//        priority: 1,
//        compile:function compile(tElement, tAttrs) {
//            return function postLink($scope, $element, attrs) {
//                // watch the expression, and update the UI on change.
//                dpd("default-pages").get({$sort: {sortOrder: 1}}, function (result) {
//                    $scope.pages = result;
//                    console.log($scope.pages);
//                    $scope.$apply();
//                });
//            }
//        }
//    };
//
//    return directiveDefinitionObject;
//});
//
//


app.directive("siteNav", function ($location) {
    return function ($scope, element, attrs) {

        function highlighter() {
//            console.log($location);
            var links = element.find('a'),
                link,
                currentLink,
                urlMap = {},
                i;

            for (i = 0; i < links.length; i++) {
                link = angular.element(links[i])
                urlMap[link.attr('href')] = link;
            }

//        console.log($location.path(), urlMap);
            currentLink = urlMap["/#!" + $location.path()];
            if (!currentLink) currentLink = urlMap["/#!"];

            if (currentLink) {
//                console.log("add active");
                currentLink.parent().addClass("active");
            }

            $scope.$on('$routeChangeStart', function () {

                var pathLink = urlMap["/#!" + $location.path()];

                if (pathLink) {
                    if (currentLink) {
                        currentLink.parent().removeClass('active');
                    }
                    currentLink = pathLink;
                    currentLink.parent().addClass('active');
                }
            });
        }

        // watch the expression, and update the UI on change.
        dpd("default-pages").get({enabled: true, $sort: {sortOrder: 1}}, function (result) {
            $scope.pages = result;
//            console.log($scope.pages);
            $scope.$apply();
            highlighter();
        });
    };

});