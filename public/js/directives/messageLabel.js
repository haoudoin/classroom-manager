/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/15/13
 * Time: 7:43 PM
 */

app.directive("messageLabel", function () {
    return function (scope, element, attrs) {
        var oldHtml = element.html();
//        console.log(oldHtml);
        var messageQueries = attrs.ngBind.split(".");
//        console.log(attrs.ngJson)
        var query = angular.fromJson(attrs.ngJson || undefined);
//        console.log(query)
        // default-pages
        dpd(messageQueries[0]).get(query, function (result) {
            if (result.length > 0) {
                element.html(result[0][messageQueries[1]])
            } else {
                element.html(oldHtml)
            }
        });
    }
});

app.directive("siteTheme", function () {
    return function (scope, element, attrs) {
        dpd.site.get(function(siteInfo){
            if(siteInfo.length > 0 && siteInfo[0].siteTheme != "") {
                element.attr("href", siteInfo[0].siteTheme)
            }
        });
    }
});



app.directive("activeToggle", function () {
    return function(scope, element, attrs ) {
        angular.element(element).find(".active").removeClass("active");

    };
});