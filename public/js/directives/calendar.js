/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/21/13
 * Time: 7:22 PM
 */
app.directive("calendar", function () {
    return function (scope, element, attrs) {
        dpd.calendarfeeds.get(function(feeds ) {
            console.log(feeds);
            scope.uiConfig.calendar.eventSources = feeds;
            console.log(scope.uiConfig.calendar);
            $(element).fullCalendar(scope.uiConfig.calendar);
            setTimeout(function(){$(element).fullCalendar("render");}, 1000);
        });
    };
});
