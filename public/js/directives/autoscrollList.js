/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/26/13
 * Time: 1:15 AM
 */
app.directive('autoscrollList', function ($timeout) {
    return {
        link: function postLink(scope, element, attrs) {
            var scrollFunction = function () {
                $(element).animate({
                    scrollTop: element.prop('scrollHeight')
                }, 1000);
                timeoutId = undefined;
            };

            var timeoutId;

            scope.$watch(
                function () {
                    return element.children().length;
                },
                function () {
                    if (timeoutId) {
                        $timeout.flush(timeoutId);
                    }
                    timeoutId = $timeout(scrollFunction, 50);
                }
            );
        }
    };
});