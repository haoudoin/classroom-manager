/**
 * Created with IntelliJ IDEA.
 * User: Hao
 * Date: 9/15/13
 * Time: 7:42 PM
 */

app.directive('dpdMessageFor', function () {
    return function (scope, element, attrs) {
        var post = scope.$eval(attrs.dpdMessageFor);
        var message = post.message;
        var mentions = post.mentions;
        if (mentions) {
            mentions.forEach(function (m) {
                message = message.replace('@' + m, '<a href="/#/profile?user=' + m + '">@' + m + '</a>');
            });
        }

        element.html(message);
    };
});
