// production.js
var deployd = require('deployd');

var server = deployd({
    port: 5000,
    env: 'production',
    db: {
        host: 'localhost',
        port: 27017,
        name: 'classroom_manager'
//    credentials: {
//      username: 'root',
//      password: 'b4fcf10e0218ba57a89288fab7c4a410'
//    }
    }
});

server.listen();

server.on('listening', function () {
    console.log("Server is listening");
});

server.on('error', function (err) {
    console.error(err);
    process.nextTick(function () { // Give the server a chance to return an error
        process.exit();
    });
});

