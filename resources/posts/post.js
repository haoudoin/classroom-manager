if (!me) cancel("You must be logged in to post", 401);
if (!me.role=="admin") cancel("You do not have permission to posts announcements.", 401);

this.creator = me.id;
this.creatorName = me.username;

this.postedTime = new Date().getTime();

emit('posts:create', this);