if (!(me && me.id === this.creator)) {
    cancel("You are not the creator", 401);
}
